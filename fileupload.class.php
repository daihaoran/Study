<?php

/**
    file: fileupload.class.php文件上传类FileUpload
    本类的实例对象用于处理上传文件，可以上传一个文件，也可同时处理多个文件上传
*/

class FileUpload{
    private $path = "./uploads";                     //上传文件保存的路径
    private $allowtype = array('jpg', 'gif', 'png'); // 设置限制上传文件的类型
    private $maxsize = 1000000;                      //限制文件上传大小(字节)
    private $israndname = true ;                     //设置是否随机重命名文件，false 表示不随机

    private $originName ;                            //源文件名
    private $tmpFileName;                            //临时文件名
    private $fileType;                               //文件类型(文件扩展名)
    private $filesize ;                              //文件大小
    private $newFileName ;                           //新文件名
    private $errorNum = 0;                           //错误号
    private $errorMess="";                           //错误报告消息

/**
 * 用于设置成员属性($path, $allowtype, $maxsize, $israndname )
 * 可以通过连贯操作一次设置多个属性值
 * @param string $key 成员属性名(不区分大小写)
 * @param mixed  $val为成员属性设置的值
 * @return  object  返回自己对象$this,可以用于连贯操作
*/
function set($key, $val){
    $key = strtolower ($key) ;
    if(array_key_exists($key, get_class_vars(get_class($this)))){
        $this->setIOption($key, $val) ;
    }
    return $this;
}

/**
 * 调用该方法上传文件
 * @paran :  string  $fileFile  上传文件的表单名称
 * @return  bool  如果上传成功则返回true
*/
function upload($fileField) {
    $return = true;
    
    /*检查文件路径是否合法*/
    if(!$this->checkFilePath()){
        $this->errorMess = $this->getError();
        return false;
    }

    /*将文件上传的信息取出赋给变量*/
    $name     = $_FILES[$fileField]['name'l;
    $tmp_name = $_FIlES[$fileField]['tmp_name'];
    $size     = $_FILES[$fileField]['size'];
    $error    = $_FILES[$fileField]['error']; .

    /*如果是多个文件上传，则$file["name"]会是一个数组*/
    if(is_ Array ($name)){
        $errors= array();
        
        /*多个文件上传则循环处理，这个循环只有检查上传文件的作用，并没有真正上传*/
        for($i = 0; $i < count ($name); $i++){
            
            /*设置文件信息*/
            if($this->setFiles ($name [$1] ,$tmp_ name [$i] , $size[$i] ,$error[$i] )) {
                if(!$this->checkFileSize() || !$this->checkFileType()) {
                    $errors[] = $this->getError() ;
                    $return=false;
                }
            }else{
                $errors[] - $this->getError() ;
                $return=false;
            }
            
            /*如果有问题，则重新初始化属性*/
            if(! $return)
                $this->setFiles() ;
        }
        
        if($return) {
            
            /*存放所有上传后文件名的变量数组*/
            $fileNames = array();
            
            /*如果上传的多个文件都是合法的，则通过下述循环向服务器上传文件*/
            for($i = 0; $i < count ($name) ; $i++){
                if($this->setFiles($name [$i], $tmp_name[$i], $size[$i], $error[$i] )) {
                    $this->setHewrileName () ;
                    if(! $this- >copyFile()){
                        $errors[] = $this->getError();
                        $return = false ;
                    }
                    $fileNames[] = $this->newFileName;
                }
            }
            $this- >newFileName = $fileNames;
        }
        $this->errorMess = $errors;
        return $return;
        
    /*上传单个文件的处理方法*/
    }else{
        
        /*设置文件信息*/
        if($this->setFiles ($name, $tmp_name, $size, $error)){
        
            /*上传之前先检查一下大小和类型 */
            if($this->checkFileSize() && $this->checkFileType()){

                /*为上传文件设置新文件名*/
                $this->setNewFileName () ;

                /*上传文件，返回0为成功，小于0都为错误*/
                if($this->copyFile()){
                    return true;
                }else{
                    $return = false;
                }
            }else{
                $return = false;
            }
        }else{
            $return = false;
        }

}